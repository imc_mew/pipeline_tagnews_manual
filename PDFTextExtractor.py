import pandas as pd
import requests
import fitz  # PyMuPDF
from io import BytesIO
import re
from sklearn.base import BaseEstimator, TransformerMixin

class PDFTextExtractor(BaseEstimator, TransformerMixin):
    def __init__(self, fix_sara_am_func=None):
        self.fix_sara_am_func = fix_sara_am_func

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        # Assuming X is a DataFrame with a column named 'PDF Links'
        X_transformed = X.copy()
        X_transformed['Text from PDF'] = ''

        for index, row in X_transformed.iterrows():
            if row['PDF Links']:
                text_content = ''
                for pdf_url in row['PDF Links']:
                    try:
                        response = requests.get(pdf_url)
                        response.raise_for_status()
                        with BytesIO(response.content) as open_pdf_file:
                            pdf_document = fitz.open("pdf", open_pdf_file.read())
                            for page in pdf_document:
                                page_text = page.get_text()
                                if page_text:
                                    text_content += page_text
                            pdf_document.close()
                    except Exception as e:
                        continue  # Skip to next PDF

                if text_content and self.fix_sara_am_func:
                    text_content = self.fix_sara_am_func(text_content)
                X_transformed.at[index, 'Text from PDF'] = text_content
        return X_transformed

# Example usage
# Define the fix_sara_am function outside of the class
def fix_sara_am(text):
    fixed_text = re.sub(r'([ก-ฮ])([่้๊๋]?)( า)', r'\1\2ำ', text)
    return fixed_text