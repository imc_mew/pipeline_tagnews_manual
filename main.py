import pandas as pd
from pythainlp.tokenize import word_tokenize
from pythainlp.corpus import thai_stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.corpus import stopwords
import pickle
from ExtractPDFLinks import ExtractPDFLinks
from MergeAndCleanText import MergeAndCleanText
from PDFLanguageDetector import PDFLanguageDetector
from RemoveInvalidPDFLinks import RemoveInvalidPDFLinks
from TextPreprocessor import TextPreprocessor
from PDFTextExtractor import PDFTextExtractor
from sklearn.pipeline import Pipeline

# nltk.download('stopwords')
# with open('C:/Users/vadhi/imc_mew/pipeline/tfidf_vectorizer.pkl', 'rb') as file:
#     loaded_tfidf_vectorizer = pickle.load(file)
# ###
# # file_path = 'C:/Users/vadhi/imc_mew/pipeline/Book1.csv'
# # data = pd.read_csv(file_path)
# data = []
# while True:
#     # รับข้อมูลจากผู้ใช้
#     post_title = input("Enter Post Title (or type 'done' to finish): ")
#     if post_title == 'done':
#         break  # ออกจาก loop หากผู้ใช้พิมพ์ 'done'
#     post_content = input("Enter Post Content: ")
    
#     # เพิ่มข้อมูลไปยัง list
#     data.append({
#         "Post Title": post_title,
#         "Post Content": post_content,
#     })
# df_test = pd.DataFrame(data)
# df_test["Detected Language"]=""
# print(df_test)
# ###
# pipeline = Pipeline([
#     ('extract_pdf_links', ExtractPDFLinks()),  # ดึงลิงก์ PDF
#     ('pdf_language_detector', PDFLanguageDetector()),  # ตรวจจับภาษาของ PDF
#     ('remove_invalid_pdf_links', RemoveInvalidPDFLinks()),  # ลบลิงก์ PDF ที่ไม่ต้องการ
#     ('pdf_text_extractor', PDFTextExtractor()),  # ดึงข้อความจาก PDF
#     ('merge_and_clean_text', MergeAndCleanText()),  # รวมและทำความสะอาดข้อความ
#     ('text_preprocessor', TextPreprocessor()),

# ])
# #
# # new_data_transformed = pipeline.fit_transform(data)
# new_data_transformed = pipeline.fit_transform(df_test)
# X_test = loaded_tfidf_vectorizer.transform(new_data_transformed['Merged Text'])

# with open('C:/Users/vadhi/imc_mew/pipeline/modelfinal.pkl', 'rb') as file:
#     model = pickle.load(file)
# model_predictions = model.predict(X_test)
# columns_to_add = ['New Economy', 'การค้าระหว่างประเทศ', 'การลงทุนระหว่างประเทศ',
#                   'กุ้งขาวแวนนาไม', 'ข้าว', 'ข้าวโพดเลี้ยงสัตว์', 'ถั่วเหลือง',
#                   'ทุเรียน', 'ธุรกิจบริการ', 'ธุรกิจบริการอื่นๆ', 'ธุรกิจสุขภาพ',
#                   'ธุรกิจโลจิสติกส์', 'ปาล์มน้ำมัน', 'มันสำปะหลัง', 'ยางพารา',
#                   'ยานยนต์และส่วนประกอบ', 'วัสดุก่อสร้าง', 'สับปะรด', 'สุกร', 'อาหาร',
#                   'อาหารอื่นๆ', 'อาหารแปรรูป', 'อิเล็กทรอนิกส์และดิจิทัล', 'อื่นๆ',
#                   'อุตสาหกรรม', 'อุตสาหกรรมอื่นๆ', 'เกษตร', 'เกษตรอื่นๆ',
#                   'เคมีภัณฑ์และพลาสติก', 'เครื่องดื่ม', 'เศรษฐกิจการค้าระหว่างประเทศ',
#                   'เศรษฐกิจประเทศคู่ค้า', 'โคนม', 'ไก่เนื้อ', 'ไก่ไข่']


# dpredictions_df = pd.DataFrame(model_predictions, columns=columns_to_add)
# dpredictions_df.insert(0, 'Merged Text', new_data_transformed['Merged Text'])
# dpredictions_df['Related Categories'] = ''
# print(dpredictions_df)
# print("==============================================================================")
# for index, row in dpredictions_df.iterrows():
#     # รวบรวมชื่อหมวดหมู่ที่มีค่าเป็น 1
#     categories = [category for category, value in row[1:].items() if value == 1]
#     # รวมชื่อหมวดหมู่ที่คัดเลือกเป็น string และเพิ่มไปยังคอลัมน์ 'Related Categories'
#     if not categories:  # ถ้าไม่มีหมวดหมู่ใดที่มีค่าเป็น 1
#         dpredictions_df.at[index, 'Related Categories'] = 'ไม่มีหมวดหมู่ข่าว'
#     else:
#         # รวมชื่อหมวดหมู่ที่คัดเลือกเป็น string และเพิ่มไปยังคอลัมน์ 'Related Categories'
#         dpredictions_df.at[index, 'Related Categories'] = ', '.join(categories)
# final_output = dpredictions_df[['Merged Text', 'Related Categories']]
# print(final_output)

# ตั้งค่า nltk
nltk.download('stopwords')

def thai_tokenizer_and_remove_stopwords(text):
    thai_stopwords_set = set(thai_stopwords())
    eng_stopwords_set = set(stopwords.words('english'))
    all_stopwords = thai_stopwords_set.union(eng_stopwords_set)

    tokens = word_tokenize(text, engine='newmm')
    return [word for word in tokens if word not in all_stopwords]

# ฟังก์ชันสำหรับเลือก อัพโหลดข้อมูลแบบ csv และ manual
def load_data():
    load_option = input("Enter 'csv' to load data from a CSV file, or 'input' to enter data manually: ")

    if load_option == 'csv':
        file_path = input("Enter the path to your CSV file: ")
        return pd.read_csv(file_path)
    elif load_option == 'input':
        data = []
        while True:
            post_title = input("Enter Post Title (or type 'done' to finish): ")
            if post_title == 'done':
                break
            post_content = input("Enter Post Content: ")
            data.append({
                "Post Title": post_title,
                "Post Content": post_content,
            })
        return pd.DataFrame(data)
    else:
        print("Invalid option selected.")
        return pd.DataFrame()

data = load_data()

if not data.empty:
    df_test = data.copy()
    df_test["Detected Language"] = ""
    
    # โค้ดสำหรับการตั้งค่า pipeline
    # สมมติว่าคุณมีไฟล์และคลาสที่จำเป็นทั้งหมดใน project ของคุณ
    pipeline = Pipeline([
        ('extract_pdf_links', ExtractPDFLinks()),  # ดึงลิงก์ PDF
        ('pdf_language_detector', PDFLanguageDetector()),  # ตรวจจับภาษาของ PDF
        ('remove_invalid_pdf_links', RemoveInvalidPDFLinks()),  # ลบลิงก์ PDF ที่ไม่ต้องการ
        ('pdf_text_extractor', PDFTextExtractor()),  # ดึงข้อความจาก PDF
        ('merge_and_clean_text', MergeAndCleanText()),  # รวมและทำความสะอาดข้อความ
        ('text_preprocessor', TextPreprocessor()),
    ])
    
    new_data_transformed = pipeline.fit_transform(df_test)
    
    # โหลด TF-IDF Vectorizer
    with open('C:/Users/vadhi/imc_mew/pipeline/tfidf_vectorizer.pkl', 'rb') as file:
        loaded_tfidf_vectorizer = pickle.load(file)
    
    X_test = loaded_tfidf_vectorizer.transform(new_data_transformed['Merged Text'])
    
    # โหลด model
    with open('C:/Users/vadhi/imc_mew/pipeline/modelfinal.pkl', 'rb') as file:
        model = pickle.load(file)
    
    model_predictions = model.predict(X_test)
    
    # คอลัมน์สำหรับเพิ่ม
    columns_to_add = ['New Economy', 'การค้าระหว่างประเทศ', 'การลงทุนระหว่างประเทศ',
                      'กุ้งขาวแวนนาไม', 'ข้าว', 'ข้าวโพดเลี้ยงสัตว์', 'ถั่วเหลือง',
                      'ทุเรียน', 'ธุรกิจบริการ', 'ธุรกิจบริการอื่นๆ', 'ธุรกิจสุขภาพ',
                      'ธุรกิจโลจิสติกส์', 'ปาล์มน้ำมัน', 'มันสำปะหลัง', 'ยางพารา',
                      'ยานยนต์และส่วนประกอบ', 'วัสดุก่อสร้าง', 'สับปะรด', 'สุกร', 'อาหาร',
                      'อาหารอื่นๆ', 'อาหารแปรรูป', 'อิเล็กทรอนิกส์และดิจิทัล', 'อื่นๆ',
                      'อุตสาหกรรม', 'อุตสาหกรรมอื่นๆ', 'เกษตร', 'เกษตรอื่นๆ',
                      'เคมีภัณฑ์และพลาสติก', 'เครื่องดื่ม', 'เศรษฐกิจการค้าระหว่างประเทศ',
                      'เศรษฐกิจประเทศคู่ค้า', 'โคนม', 'ไก่เนื้อ', 'ไก่ไข่']
    
    dpredictions_df = pd.DataFrame(model_predictions, columns=columns_to_add)
    dpredictions_df.insert(0, 'Merged Text', new_data_transformed['Merged Text'])
    
    # ประมวลผลหมวดหมู่ที่เกี่ยวข้อง
    for index, row in dpredictions_df.iterrows():
        categories = [category for category, value in row[1:].items() if value == 1]
        if not categories:
            dpredictions_df.at[index, 'Related Categories'] = 'ไม่มีหมวดหมู่ข่าว'
        else:
            dpredictions_df.at[index, 'Related Categories'] = ', '.join(categories)
    
    final_output = dpredictions_df[['Merged Text', 'Related Categories']]
    print(final_output)
else:
    print("No data available for processing.")
