from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd

class RemoveInvalidPDFLinks(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        # Transformer doesn't need to learn anything from the data
        return self

    def transform(self, X):
        # Assuming X is a DataFrame with columns 'Detected Language' and 'PDF Links'    
        condition = X['Detected Language'].isnull() | (~X['Detected Language'].isin(['en', 'th']))
        X.loc[condition, 'PDF Links'] = ''
        return X