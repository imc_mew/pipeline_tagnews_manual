import re
from bs4 import BeautifulSoup
from pythainlp.corpus import thai_stopwords
from pythainlp.tokenize import word_tokenize
import string
from sklearn.base import BaseEstimator, TransformerMixin

class TextPreprocessor(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.thai_stopwords = thai_stopwords()

    def clean_html(self, text):
        soup = BeautifulSoup(text, "html.parser")
        text = soup.get_text()
        text = re.sub(r'[^ก-๙a-zA-Z0-9\s]', '', text)
        text = re.sub(r'\d+', '', text)
        text = text.replace('nan', '')
        return text

    def remove_urls(self, text):
        url_pattern = r'https?://\S+|www\.\S+'
        return re.sub(url_pattern, '', text)

    def tokenize_and_remove_stopwords(self, text):
        text = re.sub(r'\n|\xa0|\xa0\xa0|\t', ' ', text)
        text = re.sub(r'\s+', ' ', text).strip()
        tokens = word_tokenize(text, keep_whitespace=False, engine="newmm")

        filtered_tokens = [word for word in tokens if word not in self.thai_stopwords and word not in string.punctuation and word != 'X']
        return ' '.join(filtered_tokens)

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        X_transformed = X.copy()
        X_transformed['Merged Text'] = X_transformed['Merged Text'].apply(lambda x: self.clean_html(str(x)))
        X_transformed['Merged Text'] = X_transformed['Merged Text'].apply(lambda x: self.tokenize_and_remove_stopwords(str(x)))
        X_transformed['Merged Text'] = X_transformed['Merged Text'].apply(lambda x: self.remove_urls(str(x)))
        return X_transformed