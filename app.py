from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import pandas as pd
import pickle
from sklearn.pipeline import Pipeline
from ExtractPDFLinks import ExtractPDFLinks
from PDFLanguageDetector import PDFLanguageDetector
from RemoveInvalidPDFLinks import RemoveInvalidPDFLinks
from PDFTextExtractor import PDFTextExtractor
from MergeAndCleanText import MergeAndCleanText
from TextPreprocessor import TextPreprocessor

app = FastAPI()

class PostData(BaseModel):
    posts: list[dict]   

@app.post("/predict/")
async def predict(post_data: PostData):
    try:
        # Preprocess data
        data = pd.DataFrame(post_data.posts)
        pipeline = Pipeline([
            ('extract_pdf_links', ExtractPDFLinks()),
            ('pdf_language_detector', PDFLanguageDetector()),
            ('remove_invalid_pdf_links', RemoveInvalidPDFLinks()),
            ('pdf_text_extractor', PDFTextExtractor()),
            ('merge_and_clean_text', MergeAndCleanText()),
            ('text_preprocessor', TextPreprocessor()),
        ])
        new_data_transformed = pipeline.fit_transform(data)

        # Load TF-IDF Vectorizer
        with open('D:/IMC/newpipelineManual/pipeline_tagnews_manual/tfidf_vectorizernotoken.pkl', 'rb') as file:
            loaded_tfidf_vectorizer = pickle.load(file)

        X_test = loaded_tfidf_vectorizer.transform(new_data_transformed['Merged Text'])

        # Load model
        with open('D:/IMC/newpipelineManual/pipeline_tagnews_manual/modelfixtf.pkl', 'rb') as file:
            model = pickle.load(file)

        model_predictions = model.predict(X_test)

        # Add predictions to DataFrame
        columns_to_add = ['New Economy', 'การค้าระหว่างประเทศ', 'การลงทุนระหว่างประเทศ',
                          'กุ้งขาวแวนนาไม', 'ข้าว', 'ข้าวโพดเลี้ยงสัตว์', 'ถั่วเหลือง',
                          'ทุเรียน', 'ธุรกิจบริการ', 'ธุรกิจบริการอื่นๆ', 'ธุรกิจสุขภาพ',
                          'ธุรกิจโลจิสติกส์', 'ปาล์มน้ำมัน', 'มันสำปะหลัง', 'ยางพารา',
                          'ยานยนต์และส่วนประกอบ', 'วัสดุก่อสร้าง', 'สับปะรด', 'สุกร', 'อาหาร',
                          'อาหารอื่นๆ', 'อาหารแปรรูป', 'อิเล็กทรอนิกส์และดิจิทัล', 'อื่นๆ',
                          'อุตสาหกรรม', 'อุตสาหกรรมอื่นๆ', 'เกษตร', 'เกษตรอื่นๆ',
                          'เคมีภัณฑ์และพลาสติก', 'เครื่องดื่ม', 'เศรษฐกิจการค้าระหว่างประเทศ',
                          'เศรษฐกิจประเทศคู่ค้า', 'โคนม', 'ไก่ไข่','ไก่เนื้อ']
        dpredictions_df = pd.DataFrame(model_predictions, columns=columns_to_add)
        dpredictions_df.insert(0, 'Merged Text', new_data_transformed['Merged Text'])

        # Process related categories
        for index, row in dpredictions_df.iterrows():
            categories = [category for category, value in row[1:].items() if value == 1]
            if not categories:
                dpredictions_df.at[index, 'Related Categories'] = 'ไม่มีหมวดหมู่ข่าว'
            else:
                dpredictions_df.at[index, 'Related Categories'] = ', '.join(categories)

        final_output = dpredictions_df[['Merged Text', 'Related Categories']]
        return final_output.to_dict(orient="records")
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))
