from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import re

class ExtractPDFLinks(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        # Transformer doesn't need to learn anything from the data
        return self

    def transform(self, X):
        # Assuming X is a DataFrame with a column named 'Post Content'
        def extract_pdf_links(content):
            pdf_links = re.findall(r'href=["\'](.*?\.pdf)["\']|src=["\'](.*?\.pdf)["\']', content)
            pdf_links = [link for t in pdf_links for link in t if link]

            updated_links = []
            for pdf_link in pdf_links:
                actual_pdf_link = re.search(r'file=(https?://.*?\.pdf)|href="(https?://.*?\.pdf)', pdf_link)
                if actual_pdf_link:
                    actual_pdf_link = actual_pdf_link.group(1) or actual_pdf_link.group(2)
                    updated_links.append(actual_pdf_link)
                else:
                    updated_links.append(pdf_link)

            return updated_links

        X_transformed = X.copy()
        X_transformed['PDF Links'] = X['Post Content'].apply(extract_pdf_links)
        return X_transformed