import pandas as pd
import requests
from io import BytesIO
from PyPDF2 import PdfReader
from langdetect import detect
from sklearn.base import BaseEstimator, TransformerMixin

class PDFLanguageDetector(BaseEstimator, TransformerMixin):
    def __init__(self, timeout=20):
        self.timeout = timeout

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        # Assuming X is a DataFrame with columns 'PDF Links' and 'Detected Language'

        def extract_text_from_pdf(url):
            try:
                response = requests.get(url, timeout=self.timeout)
                response.raise_for_status()
                with BytesIO(response.content) as open_pdf_file:
                    reader = PdfReader(open_pdf_file)
                    text = ""
                    for page in reader.pages:
                        text += (page.extract_text() or "") + "\n"
                    return text.strip()
            except Exception as e:
                print(f"Error extracting text from PDF: {e}")
                return None

        def detect_language(text):
            try:
                return detect(text)
            except:
                print("Error detecting language")
                return None

        X_transformed = X.copy()
        for index, row in X_transformed.iterrows():
            pdf_url = row['PDF Links']
            pdf_text = extract_text_from_pdf(pdf_url)
            if pdf_text:
                detected_language = detect_language(pdf_text)
                X_transformed.at[index, 'Detected Language'] = detected_language
            else:
                # Optionally handle the case where PDF text could not be extracted
                X_transformed.at[index, 'Detected Language'] = None

        return X_transformed
