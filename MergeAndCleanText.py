from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
class MergeAndCleanText(BaseEstimator, TransformerMixin):
    def __init__(self):
        # คอลัมน์ที่จะรวมและคอลัมน์ที่จะลบ
        self.text_columns = ['Post Content', 'Text from PDF']
        self.drop_columns = ['Post Title', 'PDF Links', 'Post Content', 'Text from PDF', 'Detected Language']

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        X_transformed = X.copy()

        # รวมข้อความ
        merged_text = X_transformed[self.text_columns[0]].astype(str)
        for col in self.text_columns[1:]:
            merged_text += ' ' + X_transformed[col].astype(str)
        X_transformed['Merged Text'] = merged_text

        # ลบคอลัมน์ที่ไม่ต้องการ
        X_transformed = X_transformed.drop(columns=self.drop_columns)

        # จัดเรียงคอลัมน์
        columns = ['Merged Text'] + [col for col in X_transformed.columns if col != 'Merged Text']
        X_transformed = X_transformed[columns]

        return X_transformed